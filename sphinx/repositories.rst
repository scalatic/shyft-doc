*********************
What is a repository
*********************
A repository in Shyft context provides a minimal interface to a service, that can be used to compose and orchestrate functionality as required by the business.
It typically hides a way non-essential details for the user, e.g. how information/things are stored/retrieved from backing services and storage layers.

Parameters and responses of a repository interfaces are either basic types like int,string, or domain-objects that can directly be used by the calling code.