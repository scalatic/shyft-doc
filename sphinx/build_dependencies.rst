*************************
Shyft build dependencies
*************************

Shyft builds on both linux distros and windows, and is using recent/updated compilers and frameworks.

The 3rd party dependencies is build using automated scripts and located in the shyft_dependencies directory, parallell to the shyft,shyft-data and shyft-doc directories.

The build script for this is located in shyft/build_support, name build_dependencies.sh and win_build_dependencies.sh.

The purpose of the scripts is to create a structure below shyft_dependencies that is similar to the one that you will
find on the /usr with respect to include and lib. This allow you to build packages that is missing from the system,
either because your distro does not have updated versions, or that the package is not available on your distro.

For distros like Arch-linux, we could have dropped shyft_dependencies, and just using pacman to install the dependencies,
other distros do have a quite decent update of tools and libraries. To ensure we cover most cases, we have established the
build scripts to take care of this.

Before you can execute those scripts, you need to install compilers and tools needed.

A typical setup from 'iso-install' of a linux distro goes like this:

* install gcc (8-series, or latest)
* install cmake (3.15, or better latest)
* install git (latest)
* install blas/lapack developement libraries
* install openssl development libraries

Then the script below would be typical for first time setup:

.. code-block::bash

    mkdir $HOME/projects && cd $HOME/projects
    git clone https://gitlab.com/shyft-os/shyft.git
    bash shyft/build_support/build_dependencies.sh
    source miniconda/etc/profile.d/conda.sh
    conda activate base
    mkdir shyft/build
    cd shyft/build
    cmake ..
    make -j 4 install
    export SHYFT_DATA=$HOME/projects/shyft-data
    make test
    conda activate shyft_env
    export PYTHONPATH=$HOME/projects/shyft
    cd $HOME/projects/shyft
    nosetests test_suites



Compilers and tools
====================

* python : 3.6+ , note that we download and use conda versions in the build-scripts

* c++: gcc-8 series, ms c++ vs 2017 latest service packs/patches

* cmake: latest, currently 3.15

* git: including lfs support

* wget/curl : the script uses these to download, on win it's included in the git package


System provided libraries
=========================

* blas lapack:  development versions

* openssl : on linux, use system package manager, on windows we use conda distro


C++ 3rd party libraries
========================

* boost : https://www.boost.org/

* armadillo : http://arma.sourceforge.net/

* dlib : http://dlib.net/

* doctest : https://github.com/onqtam/doctest

In addition we have also added libraries that we are tracking/planning to use:

* pybind11: https://github.com/pybind/pybind11

* otl v4 : http://otl.sourceforge.net/
